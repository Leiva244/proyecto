$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $(".carousel").carousel({
    interval: 2700,
  });

  $("#suscribe").on("show.bs.modal", function (e) {
    console.log("El modal suscribirme se está mostrando");
    $("#suscribeBtn").removeClass("btn-primary");
    $("#suscribeBtn").addClass("btn-outline-primary");
    $("#suscribeBtn").prop("disabled", true);
  });

  $("#suscribe").on("shown.bs.modal", function (e) {
    console.log("El modal suscribirme se mostró");
  });

  $("#suscribe").on("hide.bs.modal", function (e) {
    console.log("El modal suscribirme se oculta");
  });

  $("#suscribe").on("hidden.bs.modal", function (e) {
    console.log("El modal suscribirme se ocultó");
    $("#suscribeBtn").removeClass("btn-outline-primary");
    $("#suscribeBtn").addClass("btn-primary");
    $("#suscribeBtn").prop("disabled", false);
  });
});
